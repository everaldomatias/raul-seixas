<?php
add_action( 'init', 'cpt_diario' );
/**
 * Registra o custom post type Diário.
 *
 * @link http://codex.wordpress.org/Function_Reference/register_post_type
 */
function cpt_diario() {
	$labels = array(
		'name'               => _x( 'Diário', 'post type general name', 'raul' ),
		'singular_name'      => _x( 'Diário', 'post type singular name', 'raul' ),
		'menu_name'          => _x( 'Diário', 'admin menu', 'raul' ),
		'name_admin_bar'     => _x( 'Diário', 'add new on admin bar', 'raul' ),
		'add_new'            => _x( 'Add New', 'book', 'raul' ),
		'add_new_item'       => __( 'Add New Diário', 'raul' ),
		'new_item'           => __( 'New Diário', 'raul' ),
		'edit_item'          => __( 'Edit Diário', 'raul' ),
		'view_item'          => __( 'View Diário', 'raul' ),
		'all_items'          => __( 'All Diário', 'raul' ),
		'search_items'       => __( 'Search Diário', 'raul' ),
		'parent_item_colon'  => __( 'Parent Diário:', 'raul' ),
		'not_found'          => __( 'No diário found.', 'raul' ),
		'not_found_in_trash' => __( 'No diário found in Trash.', 'raul' )
	);

	$args = array(
		'labels'             => $labels,
        'description'        => __( 'Description.', 'raul' ),
		'public'             => true,
		'publicly_queryable' => true,
		'show_ui'            => true,
		'show_in_menu'       => true,
		'query_var'          => true,
		'rewrite'            => array( 'slug' => 'diario' ),
		'capability_type'    => 'post',
		'has_archive'        => true,
		'hierarchical'       => false,
		'menu_position'      => null,
		'menu_icon'			 => 'dashicons-welcome-widgets-menus',
		'supports'           => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'comments' )
	);

	register_post_type( 'diario', $args );
}